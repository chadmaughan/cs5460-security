/*
 * install libpcap-dev (Ubuntu: apt-get install libpcap-dev)
 * To add PCAP libraries to Eclipse CDT:
 *
 * - Right-click on project,
 * - Select C/C++ Build -> Settings
 * - On Tool Settings tab, select GCC C++ Linker -> Libraries
 * - click '+' button then type in "pcap"
 */
#include <stdio.h>
#include <pcap.h>
#include <stdlib.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <set>
#include <map>
#include <string>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <iostream>

using namespace std;

struct sockaddr_in source, destination;

// ethernet header types
#define	ETHERTYPE_PUP		0x0200	/* PUP protocol */
#define	ETHERTYPE_IP		0x0800	/* IP protocol */
#define	ETHERTYPE_ARP		0x0806	/* Addr. resolution protocol */
#define	ETHERTYPE_REVARP	0x8035	/* reverse Addr. resolution protocol */
#define	ETHERTYPE_VLAN		0x8100	/* IEEE 802.1Q VLAN tagging */
#define	ETHERTYPE_IPV6		0x86dd	/* IPv6 */
#define	ETHERTYPE_LOOPBACK	0x9000	/* used to test interfaces */

#define SIZE_ETHERNET 14

const struct ethernet_header *ethernet;
const struct ip_header *ip;
const struct tcp_header *tcp;
const struct udp_header *udp;
const struct icmp_header *icmp;
const char *payload;

u_int size_ip;
u_int size_tcp;

// ethernet addresses = 6 bytes
#define ETHER_ADDR_LEN	6

// ethernet header
struct ethernet_header {
	u_char ether_dhost[ETHER_ADDR_LEN]; /* Destination host address */
	u_char ether_shost[ETHER_ADDR_LEN]; /* Source host address */
	u_short ether_type; 				/* IP? ARP? RARP? etc */
};

// ip header
struct ip_header {
	u_char version;
	u_char type_of_service;
	u_short total_length;
	u_short identification;
	u_short offset;
	u_char time_to_live;
	u_char protocol;
	u_short checksum;
	struct in_addr source, destination;
	#define IP_RF 0x8000		/* reserved fragment flag */
	#define IP_DF 0x4000		/* don't fragment flag */
	#define IP_MF 0x2000		/* more fragments flag */
	#define IP_OFFMASK 0x1fff	/* mask for fragmenting bits */
};

#define IP_HL(ip)		(((ip)->version) & 0x0f)
#define IP_V(ip)		(((ip)->version) >> 4)

// udp header
struct udp_header {
	u_short source_port;
	u_short destination_port;
	u_short length;
	u_short checksum;
};

// tcp header
struct tcp_header {
	u_short source;
	u_short destination;
	u_int sequence;
	u_int acknowledgment;

	u_char offset;
	u_char flags;
	u_short window;
	u_short checksum;
	u_short urgent;

	#define TH_OFF(th)	(((th)->offset & 0xf0) >> 4)
	#define TH_FIN 0x01
	#define TH_SYN 0x02
	#define TH_RST 0x04
	#define TH_PUSH 0x08
	#define TH_ACK 0x10
	#define TH_URG 0x20
	#define TH_ECE 0x40
	#define TH_CWR 0x80
	#define TH_FLAGS (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)
};

struct icmp_header {
  u_int8_t icmp_type;
  u_int8_t icmp_code;

  u_int16_t icmp_checksum;
  u_int16_t icmp_identifier;
  u_int16_t icmp_sequence;
};

void print_payload(const char *payload, int len);
void print_icmp(struct icmp_header const*);
void print_udp(struct udp_header const*);
void print_tcp(struct tcp_header const*);
void print_ip(struct ip_header const*);
void print_hex_ascii_line(const char *payload, int len, int offsets);
bool contains_password(const char *payload);
void display_attack_information();

FILE *fp;

// logging options
bool to_console;
bool to_file;
bool verbose;

// attack information
string attack_name;
string attack_source_ip;
int attack_source_port;
string attack_target_ip;
int attack_target_port;
double time_first_seen;
double duration_of_attack;
set<string> attack_usernames;
set<string> attack_passwords;

int main() {

	// logging options
	to_console = false;
	to_file = false;
	verbose = false;

    // launch the menu
    cout << "Please enter a PCAP network dump file:" << endl;
    string file_name;
    cin >> file_name;

    cout << "Please enter a menu choice:" << endl;
    cout << "1 - output to screen (limited information)" << endl;
    cout << "2 - output to screen and file" << endl;
    cout << "3 - detect attacks" << endl;

    string option;
    cin >> option;

    string output_file_name = "";

    if(option == "1") {
    	to_console = true;
    }
    else if(option == "2") {
    	to_file = true;
    	to_console = true;
        cout << "Enter file name (full path):" << endl;
        cin >> output_file_name;
    }
    else if(option == "3") {
    	verbose = false;
    	to_file = false;
    	to_console = false;
    }

	// logging file
	fp = fopen(output_file_name.c_str(), "w");

	// statistical counters
	unsigned int packet_counter = 0; 	// packet counter
	unsigned long byte_counter = 0; 	// total bytes seen in entire trace
	unsigned long current_counter = 0; 	// counter for current 1-second interval
	double current_ts = 0; 				// current timestamp

	double attack_start_ts = 0;

	// temporary packet buffers
	struct pcap_pkthdr header;
	const u_char *packet;

	set<string> ips;
	map<int, int> packet_count_by_ip_protocol;
	map<int, int> tcp_port_count;
	set<string> udp_source_ips;
	map<int, int> udp_port_count;

	int non_ip_packet_count = 0;
	int tcp_handshakes = 0;
	int syn_attack = 0;
	string local_ip;

	int ping_of_death = 0;
	int other_icmp = 0;

	// used for calculating port scans
	int PORT_SCAN_THRESHOLD = 20;
	string source_ip;
	int tcp_port_scan_count = 0;
	int previous_tcp_port = 0;

	// open the pcap file
	pcap_t *handle;
	char errbuf[PCAP_ERRBUF_SIZE];
	const char* fname = file_name.c_str();
	handle = pcap_open_offline(fname, errbuf);

	if (handle == NULL) {
		fprintf(stderr, "Couldn't open pcap file %s: %s\n", fname, errbuf);
		return (2);
	}

	// process each packet from the pcap file
	while ((packet = pcap_next(handle, &header))) {

		ethernet = (struct ethernet_header*) (packet);

		// keep track of the number of non-IP packets
		if(ethernet->ether_type != 8) {
			non_ip_packet_count++;
		}
		else {

			ip = (struct ip_header*) (packet + SIZE_ETHERNET);

			// first packet
			if (current_ts == 0) {
				current_ts = header.ts.tv_sec;
			}
			else if (header.ts.tv_sec > current_ts) {
				// printf("%d KBps\n", cur_counter / 1000);
				current_counter = 0;
				current_ts = header.ts.tv_sec;
			}

			size_ip = IP_HL(ip) * 4;
			if (size_ip < 20) {
				printf("   * Invalid IP header length: %u bytes\n", size_ip);
			}
			else {
				// collect the unique ip addresses (both source and destination)
				ips.insert(inet_ntoa(ip->source));
				ips.insert(inet_ntoa(ip->destination));

				// print the ip header
				print_ip(ip);

				++packet_count_by_ip_protocol[ip->protocol];

				// track for potential attack
				attack_start_ts = header.ts.tv_sec + (((double) header.ts.tv_usec) / 1000000);

				// TCP
				if(ip->protocol == 6) {

					tcp = (struct tcp_header*) (packet + SIZE_ETHERNET + size_ip);

					// print the verbose header
					print_tcp(tcp);

					// OPTION 3 - watch for port scans
					if(!(tcp->flags & TH_ACK)) {

						// account for first scan
						if(previous_tcp_port == 0) {
							previous_tcp_port = tcp->destination;
							source_ip = inet_ntoa(ip->source);
							tcp_port_scan_count++;
						}
						else {

							// is it from the same source?
							if(source_ip == inet_ntoa(ip->source)) {

								// are they hitting a different port?
								if(previous_tcp_port != tcp->destination) {
									tcp_port_scan_count++;

									if(tcp_port_scan_count > PORT_SCAN_THRESHOLD) {
										cout << "Attack warning (TCP port scan): " << inet_ntoa(ip->source) << ", " << tcp_port_scan_count << endl;
										attack_name = "port scan";
										attack_source_ip = source_ip;
										attack_source_port = tcp->source;
										attack_target_ip = inet_ntoa(ip->destination);
										attack_target_port = tcp->destination;
										time_first_seen = attack_start_ts;
										double ts = header.ts.tv_sec + (((double) header.ts.tv_usec) / 1000000);
										duration_of_attack =  (attack_start_ts - ts);
										display_attack_information();
									}
								}
								else {
									tcp_port_scan_count = 0;
								}
							}
							else {
								tcp_port_scan_count = 0;
							}
						}
					}

					// OPTION 3 - watch for SYN flood attack
					// keep track of the number of TCP handshakes (identify with the SYN flag)
					if ((tcp->flags & TH_SYN) && !(tcp->flags & TH_ACK)) {
						tcp_handshakes++;

						// what is the local ip address
						local_ip = inet_ntoa(ip->source);
					}
					else {
						if (tcp->flags & TH_ACK) {
							if(syn_attack > 20) {
								cout << "Attack warning (SYN flood): " << inet_ntoa(ip->source) << ", " << syn_attack << endl;
								attack_name = "syn flood";
								attack_source_ip = source_ip;
								attack_source_port = tcp->source;
								attack_target_ip = inet_ntoa(ip->destination);
								attack_target_port = tcp->destination;
								time_first_seen = attack_start_ts;
								double ts = header.ts.tv_sec + (((double) header.ts.tv_usec) / 1000000);
								duration_of_attack =  (attack_start_ts - ts);
								display_attack_information();
							}
							syn_attack = 0;
						}
						else {
							syn_attack++;
						}
					}

					if(inet_ntoa(ip->source) == local_ip) {
						++tcp_port_count[tcp->destination];
					}

					size_tcp = TH_OFF(tcp) * 4;
					if (size_tcp < 20) {
						printf(" - invalid TCP header length: %u bytes\n", size_tcp);
					}

					payload = (char *) (packet + SIZE_ETHERNET + size_ip + size_tcp);
					int size_payload = ntohs(ip->total_length) - (size_ip + size_tcp);

					// OPTION 3 - account hacking attack
					if(contains_password(payload)) {
						cout << "Attack warning (account hacking): " << inet_ntoa(ip->source) << endl;
						attack_name = "account hacking";
						attack_source_ip = source_ip;
						attack_source_port = tcp->source;
						attack_target_ip = inet_ntoa(ip->destination);
						attack_target_port = tcp->destination;
						time_first_seen = attack_start_ts;
						double ts = header.ts.tv_sec + (((double) header.ts.tv_usec) / 1000000);
						duration_of_attack =  (attack_start_ts - ts);
						display_attack_information();
					}

					print_payload(payload, size_payload);

					// check to see if the next second has started, for statistical purposes
					int packet_length = ntohs(ip->total_length);

					current_counter += packet_length;
					byte_counter += packet_length;
					packet_counter++;
				}
				// UDP
				else if(ip->protocol == 17) {

					udp = (struct udp_header*) (packet + SIZE_ETHERNET + size_ip);

					print_udp(udp);

					udp_source_ips.insert(inet_ntoa(ip->source));

					// keep track of the source ports
					if(inet_ntoa(ip->source) == local_ip) {
						++udp_port_count[udp->source_port];
					}

				}
				// ICMP
				else if(ip->protocol == 1) {

					icmp = (struct icmp_header*) (packet + SIZE_ETHERNET + size_ip);
				    int size_icmp = 8;
				    payload = (char *) (packet + SIZE_ETHERNET + size_ip + size_icmp);
					int size_payload = ntohs(ip->total_length) - (size_ip + size_tcp);

					print_icmp(icmp);
					print_payload(payload, size_payload);

					// OPTION 3 - check for the ping-of-death attack
					// 	http://www.ll.mit.edu/mission/communications/ist/corpora/ideval/docs/attackDB.html#pod
					//  signature is any ICMP packet over 64000 bytes
					if(ip->total_length > 64000) {
						ping_of_death++;

						cout << "Attack warning (ping of death): " << inet_ntoa(ip->source) << endl;
						attack_name = "ping of death";
						attack_source_ip = source_ip;
						attack_source_port = tcp->source;
						attack_target_ip = inet_ntoa(ip->destination);
						attack_target_port = tcp->destination;
						time_first_seen = attack_start_ts;
						double ts = header.ts.tv_sec + (((double) header.ts.tv_usec) / 1000000);
						duration_of_attack =  (attack_start_ts - ts);
						display_attack_information();

					}
				}
				else {
					printf("Unsupported protocol: %u\n", ip->protocol);
				}
			}

			if(to_file)
				fprintf(fp, "Total Bytes in Flow: %l bytes\n", byte_counter);

		}
	}

	pcap_close(handle);

	// TCP ports called
	//	for(map<int,int>::iterator ii=tcp_port_count.begin(); ii!=tcp_port_count.end(); ++ii) {
	//        int port = (*ii).first;
	//        int count = (*ii).second;
	//    	printf("TCP port: %i: %i\n", port, count);
	//	}

	// UDP ports called
	//	for(map<int,int>::iterator ii=udp_port_count.begin(); ii!=udp_port_count.end(); ++ii) {
	//        int port = (*ii).first;
	//        int count = (*ii).second;
	//    	printf("UDP port: %i: %i\n", port, count);
	//	}

	//output some statistics about the whole trace
	byte_counter /= 1e6; //convert to MB to make easier to read

	if(to_console)
		printf("Processed %d packets and %l MBytes\n", packet_counter, byte_counter);

	// option 1
	printf("Count of TCP handshakes: %d\n", tcp_handshakes);
	printf("Count of UPD sources: %d\n", udp_source_ips.size());
	printf("Count of unique IP addresses (source & destination): %d\n", ips.size());
	printf("Count of Non-IP packets: %i\n", non_ip_packet_count);

	// option 2 (file output of option 1)
	fprintf(fp, "Count of TCP handshakes: %d\n", tcp_handshakes);
	fprintf(fp, "Count of UDP sources: %d\n", udp_source_ips.size());
	fprintf(fp, "Count of unique IP addresses (source & destination): %d\n", ips.size());
	fprintf(fp, "Count of Non-IP packets: %i\n", non_ip_packet_count);

	// last in option 2
	for(map<int,int>::iterator ii=packet_count_by_ip_protocol.begin(); ii!=packet_count_by_ip_protocol.end(); ++ii) {
		int protocol = (*ii).first;
		int count = (*ii).second;
		fprintf(fp, "Count of IP packets by protocol: %i: %i\n", protocol, count);
	}

	fclose(fp);

	return 0;
}

void print_ip(struct ip_header const* ip) {

	if(to_console) {
		printf("\n\n*******************************************\n");

		printf("IP Header\n");

		if(verbose) {
			printf(" -IP Version         : %d\n", (unsigned int)ip->version);
			printf(" -IP Header Length   : %d DWORDS or %d Bytes\n",(unsigned int)ip->total_length,((unsigned int)(ip->total_length))*4);
			printf(" -Type Of Service    : %d\n",(unsigned int)ip->type_of_service);
			printf(" -IP Total Length    : %d  Bytes(Size of Packet)\n",ntohs(ip->total_length));
			printf(" -Identification     : %d\n",ntohs(ip->identification));
			printf(" -TTL                : %d\n",(unsigned int)ip->time_to_live);
			printf(" -Protocol           : %d\n",(unsigned int)ip->protocol);
			printf(" -Checksum           : %d\n",ntohs(ip->checksum));
		}

		printf(" -Source IP          : %s\n", inet_ntoa(ip->source));
		printf(" -Destination IP     : %s\n", inet_ntoa(ip->destination));
	}

	// option 2
	if(to_file) {
		fprintf(fp, "\n\n*******************************************\n");
		fprintf(fp, "IP Header\n");
		fprintf(fp, " -Source IP                       : %s\n", inet_ntoa(ip->source));
		fprintf(fp, " -Destination IP                  : %s\n", inet_ntoa(ip->destination));
	}
}

bool contains_password(const char *payload) {
	return false;
}

void print_tcp(struct tcp_header const* tcp) {

	string flags = "";
	if (tcp->flags & TH_FIN)
		flags.append("TH_FIN ");
	if (tcp->flags & TH_SYN)
		flags.append("TH_SYN ");
	if (tcp->flags & TH_RST)
		flags.append("TH_RST ");
	if (tcp->flags & TH_PUSH)
		flags.append("TH_PUSH ");
	if (tcp->flags & TH_ACK)
		flags.append("TH_ACK ");
	if (tcp->flags & TH_URG)
		flags.append("TH_URG ");
	if (tcp->flags & TH_ECE)
		flags.append("TH_ECE ");
	if (tcp->flags & TH_CWR)
		flags.append("TH_CWR ");

	if(to_console) {
		printf("TCP Header\n");
		printf(" -Sequence Number    : %u\n", ntohl(tcp->sequence));
		printf(" -Acknowledge Number : %u\n", ntohl(tcp->acknowledgment));

		if(verbose) {
			printf(" -Header Length      : %d DWORDS or %d BYTES\n", (unsigned int) tcp->offset, (unsigned int) tcp->offset * 4);
			printf(" -Flag               : %d\n", (unsigned int) tcp->flags);
			printf(" -Flags              : %s\n", flags.c_str());
			printf(" -Window             : %d\n", ntohs(tcp->window));
			printf(" -Checksum           : %d\n", ntohs(tcp->checksum));
			printf(" -Urgent Pointer     : %d\n", tcp->urgent);
		}

		printf(" -Source Port        : %u\n", ntohs(tcp->source));
		printf(" -Destination Port   : %u\n", ntohs(tcp->destination));
	}

	// option 2
	if(to_file) {
		fprintf(fp, "TCP Header\n");
		fprintf(fp, " -Sequence Number                 : %u\n", ntohl(tcp->sequence));
		fprintf(fp, " -Acknowledge Number              : %u\n", ntohl(tcp->acknowledgment));
		fprintf(fp, " -Flow Details (Source Port)      : %u\n", ntohs(tcp->source));
		fprintf(fp, " -Flow Details (Destination Port) : %u\n", ntohs(tcp->destination));
	}
}

void print_udp(struct udp_header const* udp) {

	if(to_console) {
		printf("\nUDP Header\n");
		printf(" -Source Port        : %d\n", ntohs(udp->source_port));
		printf(" -Destination Port   : %d\n", ntohs(udp->destination_port));
		printf(" -UDP Length         : %d\n", ntohs(udp->length));
		printf(" -UDP Checksum       : %d\n", ntohs(udp->checksum));
	}
}

void print_icmp(struct icmp_header const* icmp) {

	if(to_console) {
		printf("\nICMP Header\n");

		// type - check wikipedia for detailed information
		// http://en.wikipedia.org/wiki/Internet_Control_Message_Protocol

		// printf("ICMP Type :%d\n", icmp->icmp_type);
		switch (icmp->icmp_type) {
			case 8:
				printf(" -ICMP Echo Request protocol\n");
				break;
			case 0:
				printf(" -ICMP Echo Reply protocol\n");
				break;
			default:
				break;
		}

		printf(" -ICMP Code          :%d\n", icmp->icmp_code);
		printf(" -Identifier         :%d\n", icmp->icmp_identifier);
		printf(" -Sequence Number    :%d\n", icmp->icmp_sequence);
		printf(" -ICMP Checksum      :%d\n", ntohs(icmp->icmp_checksum));
	}
}

void print_payload(const char *payload, int len) {

	if(to_console) {
		if(verbose) {
			printf("Payload\n");

			int len_rem = len;
			int line_width = 16; // bytes per line
			int line_len;
			int offset = 0; // zero-based offset counter
			const char *ch = payload;

			if (len <= 0)
				return;

			// data fits on one line
			if (len <= line_width) {
				print_hex_ascii_line(ch, len, offset);
				return;
			}

			// data spans multiple lines
			for (;;) {

				// compute current line length
				line_len = line_width % len_rem;

				// print line
				print_hex_ascii_line(ch, line_len, offset);

				// compute total remaining
				len_rem = len_rem - line_len;

				// shift pointer to remaining bytes to print
				ch = ch + line_len;

				// add offset
				offset = offset + line_width;

				// check if we have line width chars or less
				if (len_rem <= line_width) {
					// print last line and get out
					print_hex_ascii_line(ch, len_rem, offset);
					break;
				}
			}
		}
	}
	return;
}

void print_hex_ascii_line(const char *payload, int len, int offset) {

	if(to_console) {
		int i;
		int gap;
		const char *ch;

		// offset
		printf("%05d   ", offset);

		// hex junk
		ch = payload;
		for (i = 0; i < len; i++) {
			printf("%02X ", *ch & 0xff);
			ch++;
			// space after 8th byte (for formatting)
			if (i == 7)
				printf(" ");
		}

		// print space to handle line less than 8 bytes
		if (len < 8)
			printf(" ");

		// pad with spaces
		if (len < 16) {
			gap = 16 - len;
			for (i = 0; i < gap; i++) {
				printf("   ");
			}
		}
		printf("   ");

		// ascii junk
		ch = payload;
		for (i = 0; i < len; i++) {
			if (isprint(*ch))
				printf("%c", *ch);
			else
				printf(".");
			ch++;
		}

		printf("\n");
	}
	return;
}

void display_attack_information() {

	cout << "Attack found (" << attack_name << "), what would you like to do?" << endl;
	cout << "1 - output to console" << endl;
	cout << "2 - output to file" << endl;

	string option;
	cin >> option;
	if(option == "1") {
		printf("Name of Attack: %s\n", attack_name.c_str());

		printf("Source IP and Port of Attack: %s : %i\n", attack_source_ip.c_str(), attack_source_port);
		printf("Attack Target IP and Ports: %s : %i\n", attack_target_ip.c_str(), attack_target_port);
		printf("Time first seen: %d\n", time_first_seen);
		printf("Duration of attack: %d\n", duration_of_attack);
		printf("Usernames:\n");
		set<string>::iterator iter;
		for (iter = attack_usernames.begin(); iter != attack_usernames.end(); ++iter) {
			cout << *iter << endl;
		}
		printf("Passwords:\n");
		set<string>::iterator piter;
		for (piter = attack_passwords.begin(); piter != attack_passwords.end(); ++piter) {
			cout << *piter << endl;
		}
	}
	else {
		string attack_file_name;
		cin >> attack_file_name;

		FILE *afp;
		afp = fopen(attack_file_name.c_str(), "w");

		fprintf(afp, "Name of Attack: %s\n", attack_name.c_str());

		fprintf(afp, "Source IP and Port of Attack: %s : %i\n", attack_source_ip.c_str(), attack_source_port);
		fprintf(afp, "Attack Target IP and Ports: %s : %i\n", attack_target_ip.c_str(), attack_target_port);
		fprintf(afp, "Time first seen: %d\n", time_first_seen);
		fprintf(afp, "Duration of attack: %d\n", duration_of_attack);

		fprintf(afp, "Usernames:\n");
		set<string>::iterator iter;
		for (iter = attack_usernames.begin(); iter != attack_usernames.end(); ++iter) {
			cout << *iter << endl;
		}

		fprintf(afp, "Passwords:\n");
		set<string>::iterator piter;
		for (piter = attack_passwords.begin(); piter != attack_passwords.end(); ++piter) {
			cout << *piter << endl;
		}

		fclose(afp);
	}
}
