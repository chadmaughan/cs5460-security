//============================================================================
// Name        : cs5460-as2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <map>
#include <cmath>
#include <stdio.h>
#include <typeinfo>
#include <vector>

using namespace std;

const int NUM_ALPHA(26);

int total_letter_count = 0;

char ALPHABET[NUM_ALPHA] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

string BIGRAMS[10] = { "EN","RE","ER","NT","TH","ON","IN","TF","AN","OR"};
string TRIGRAMS[10] = {"ENT","ION","AND","ING","IVE","TIO","FOR","OUR","THI","ONE"};

int get_position_of_letter(char letter) {
    for(int i = 1; i <= NUM_ALPHA; i++) {
        if(letter == ALPHABET[i])
            return i;
    }
    return -1;
}

char get_letter_at_position(int position) {
    return ALPHABET[position];
}

string cyclic_shift(string s, int n) {
    string result;
    result = s.substr(n) + s.substr(0,n);
    return result;
}

float get_english_frequency(char letter) {

    map<char,float> frequencies;
    frequencies['A'] = .08167;
    frequencies['B'] = .01492;
    frequencies['C'] = .02782;
    frequencies['D'] = .04253;
    frequencies['E'] = .12702;
    frequencies['F'] = .02228;
    frequencies['G'] = .02015;
    frequencies['H'] = .06094;
    frequencies['I'] = .06966;
    frequencies['J'] = .00153;
    frequencies['K'] = .00772;
    frequencies['L'] = .04025;
    frequencies['M'] = .02406;
    frequencies['N'] = .06749;
    frequencies['O'] = .07507;
    frequencies['P'] = .01929;
    frequencies['Q'] = .00095;
    frequencies['R'] = .05987;
    frequencies['S'] = .06327;
    frequencies['T'] = .09056;
    frequencies['U'] = .02758;
    frequencies['V'] = .00978;
    frequencies['W'] = .02360;
    frequencies['X'] = .00150;
    frequencies['Y'] = .01974;
    frequencies['Z'] = .00074;

    return frequencies[letter];
}

void build_vigenere_table() {

    char vig[NUM_ALPHA][NUM_ALPHA];

    for(int i=0; i<NUM_ALPHA; i++) {
        for(int j=0; j<NUM_ALPHA; j++) {
            vig[i][j]=ALPHABET[((j+i) % 26)];
        }
    }

    //    for(int i=0; i<NUM_ALPHA; i++) {
    //        for(int j=0; j<NUM_ALPHA; j++) {
    //            cout << vig[i][j];
    //        }
    //        cout << endl;
    //    }
}

string get_caesar_plaintext(string ciphertext, int shift) {
    string answer = "";
    for(unsigned int i = 0; i < ciphertext.length(); i++) {
        char c = ciphertext.at(i);
        int p = get_position_of_letter(c);
        int np = (p + shift) % 26;
        answer = answer + get_letter_at_position(np);
    }
    return answer;
}

int get_caesar_shift(string cipher, bool strict) {

    map<char,int> letter_counts;
    letter_counts['A'] = 0;
    letter_counts['B'] = 0;
    letter_counts['C'] = 0;
    letter_counts['D'] = 0;
    letter_counts['E'] = 0;
    letter_counts['F'] = 0;
    letter_counts['G'] = 0;
    letter_counts['H'] = 0;
    letter_counts['I'] = 0;
    letter_counts['J'] = 0;
    letter_counts['K'] = 0;
    letter_counts['L'] = 0;
    letter_counts['M'] = 0;
    letter_counts['N'] = 0;
    letter_counts['O'] = 0;
    letter_counts['P'] = 0;
    letter_counts['Q'] = 0;
    letter_counts['R'] = 0;
    letter_counts['S'] = 0;
    letter_counts['T'] = 0;
    letter_counts['U'] = 0;
    letter_counts['V'] = 0;
    letter_counts['W'] = 0;
    letter_counts['X'] = 0;
    letter_counts['Y'] = 0;
    letter_counts['Z'] = 0;

    map<char,float> frequencies;
    frequencies['A'] = 0.0;
    frequencies['B'] = 0.0;
    frequencies['C'] = 0.0;
    frequencies['D'] = 0.0;
    frequencies['E'] = 0.0;
    frequencies['F'] = 0.0;
    frequencies['G'] = 0.0;
    frequencies['H'] = 0.0;
    frequencies['I'] = 0.0;
    frequencies['J'] = 0.0;
    frequencies['K'] = 0.0;
    frequencies['L'] = 0.0;
    frequencies['M'] = 0.0;
    frequencies['N'] = 0.0;
    frequencies['O'] = 0.0;
    frequencies['P'] = 0.0;
    frequencies['Q'] = 0.0;
    frequencies['R'] = 0.0;
    frequencies['S'] = 0.0;
    frequencies['T'] = 0.0;
    frequencies['U'] = 0.0;
    frequencies['V'] = 0.0;
    frequencies['W'] = 0.0;
    frequencies['X'] = 0.0;
    frequencies['Y'] = 0.0;
    frequencies['Z'] = 0.0;

    int cipher_letter_count = 0;

    // count the instances of each letter
    for(unsigned int i = 0; i < cipher.length(); i++) {
        ++letter_counts[cipher.at(i)];
        ++cipher_letter_count;
    }

    total_letter_count = cipher_letter_count;
    cout << "total letter count: " << cipher_letter_count << endl;

    // calculate the cipher frequencies for each letter
    for(map<char,int>::iterator ii=letter_counts.begin(); ii!=letter_counts.end(); ++ii) {

        char letter = (*ii).first;
        int count = (*ii).second;

        float frequency = count / (total_letter_count * 1.0);

        frequencies[letter] = frequency;
        // cout << "adding frequency: " << letter << ", " << frequency << "(" << frequencies[letter] << ")" << endl;
    }

    //    Let ef(c) stand for the english frequency of some letter of the alphabet
    //    Let mf(c) stand for the frequency of some letter of the message
    //    For each possible shift s between 0 and 25:
    //    For each letter c of the alphabet
    //    Compute the sum of squares of mf((c + s) mod 26) divided by ef(c)
    float smallest_sum = -1.0;
    int best = 0;

    for(int s = 0; s < NUM_ALPHA; s++) {

        float current_sum = 0.0;

        for(int c = 0; c < NUM_ALPHA; c++) {
            int ct = (c + s) % 26;
            if(ct > 0) {
                char mc = get_letter_at_position(ct);
                float mf = frequencies[mc] * 2.0;

                char ec = get_letter_at_position(c);
                float ef = get_english_frequency(ec);

                current_sum += (mf * 2.0) / ef;
            }
        }

        if(smallest_sum == -1.0) {
            smallest_sum = current_sum;
        }

        if(current_sum < smallest_sum) {
            smallest_sum = current_sum;
            best = s;
        }
    }

    // is there a letter in the cipher text that has a similar frequency to 'E'?
    bool cipher_has_an_e = false;
    bool cipher_e_is_e = false;
    for(map<char,float>::iterator ii=frequencies.begin(); ii!=frequencies.end(); ++ii) {

        char letter = (*ii).first;
        float frequency = (*ii).second;

        if(frequency > 0.11) {
            cipher_has_an_e = true;

            if(letter == 'E') {
                cipher_e_is_e = true;
            }
        }

        // cout << "frequency: " << letter << ", " << frequency << endl;
    }

    if(cipher_has_an_e) {
        if(cipher_e_is_e) {
            cout << "no shift, transposition" << endl;
            return 0;
        }
        else {
            cout << "best shift: " << get_letter_at_position(best) << ", " << best << endl;
            return best;
        }
    }
    else {
        cout << "weird distribution, not substitution cipher" << endl;
        return -1;
    }
}

int main() {

    // launch the menu
    cout << "Please enter a menu choice:" << endl;
    cout << "1 - decipher file" << endl;

    string option;
    cin >> option;

    cout << "Enter file name (full path):" << endl;
    string fileName;
    cin >> fileName;

    // read in the file
    string contents;

    string line;
    ifstream file (fileName.c_str());
    if (file.is_open()) {
        while ( file.good() ) {
            getline (file, line);
            contents = contents + line;
        }
        file.close();
    }
    else cout << "Unable to open file";

    // get the caesar shift (0 = trasposition, > 0 = substitution)
    int shift = get_caesar_shift(contents, true);

    // TRANSPOSITION --------------------------------------------------
    // if guess(E) == E then you just have transposition
    //      see the book page 57
    if(shift == 0) {

        int max_columns = 13;

        string built_bigram;
        int top_bigram_hit_count = 0;
        int bigram_frequency[max_columns];
        float normalized_bigram_frequency[max_columns];

        string built_trigram;
        int top_trigram_hit_count = 0;
        int trigram_frequency[max_columns];
        float normalized_trigram_frequency[max_columns];

        // For efficiency, limit the number of potential columns.  I'm guessing for our purposes it'll be less than 100.
        // I'm also guessing that we'll have more than 3 columns
        //  (i represents the distance between characters)
        for(int i = 3; i < max_columns; i++) {

            int bigram_hit_count = 0;
            int trigram_hit_count = 0;

            for(unsigned int j = 0; j < contents.length(); j++) {

                // compare bigram
                if(j + i < contents.length()) {
                    built_bigram = contents.substr(j,1) + contents.substr(j + i, 1);
                    for(unsigned int k = 0; k < 10; k++) {
                        if(BIGRAMS[k] == built_bigram) {
                            bigram_hit_count++;
                            break;
                        }
                    }
                }

                // compare trigrams
                if(i + j + j < contents.length()) {
                    built_trigram = contents.substr(j,1) + contents.substr(j + i, 1) + contents.substr(j + j + i, 1);
                    for(unsigned int k = 0; k < 10; k++) {
                        if(TRIGRAMS[k] == built_trigram) {
                            trigram_hit_count++;
                            break;
                        }
                    }
                }
            }

            // record the top hit count for normalization
            bigram_frequency[i] = bigram_hit_count;
            if(bigram_hit_count > top_bigram_hit_count)
                top_bigram_hit_count = bigram_hit_count;

            trigram_frequency[i] = trigram_hit_count;
            if(trigram_hit_count > top_trigram_hit_count)
                top_trigram_hit_count = trigram_hit_count;

            // cout << i << ", " << bigram_hit_count << " - " << trigram_hit_count << endl;
        }

        // normalize the bigram hit counts
        for(int i = 3; i < max_columns; i++) {
            normalized_bigram_frequency[i] = bigram_frequency[i] / (top_bigram_hit_count * 1.0);
            cout << "normalized bigram hit (" << i << "): " << normalized_bigram_frequency[i] << endl;
        }

        // normalize the trigram hit counts
        for(int i = 3; i < max_columns; i++) {
            normalized_trigram_frequency[i] = (trigram_frequency[i] / (top_trigram_hit_count * 1.0));
            cout << "normalized trigram hit (" << i << "): " << normalized_trigram_frequency[i] << endl;
        }

        // find the top bigram & trigram frequency to determine the column split
        int num_columns = 0;
        float top_normalized_count = 0.0;
        for(int i = 3; i < max_columns; i++) {
            float normalized_count = normalized_trigram_frequency[i] + normalized_bigram_frequency[i];
            cout << i << ", " << normalized_count << endl;
            if(normalized_count > top_normalized_count) {
                top_normalized_count = normalized_count;
                num_columns = i;
            }
        }

        cout << num_columns << endl;

        // get the row count
        num_columns = 12;
        int num_rows = total_letter_count / num_columns;
        cout << num_rows << endl;

        for(int i = 0; i < num_rows; i++) {
            for(int j = 0; j < num_columns; j++) {
                int c = num_rows * j;
                cout << contents[c + i];
            }
            cout << endl;
        }
    }
    else if (shift < 0) {

        // if the shift is less than zero (caesar cipher failed), try to figure the key from the Kasiski examination
        //          http://user.it.uu.se/~olgag/Cryptology/vigenere.html
        //          http://amca01.wordpress.com/2009/11/03/vigenre-and-kasiski/

        build_vigenere_table();

        int max_key_length = 20;
        int max_coincidences_columns = 0;
        int max_coincidences_amount = 0;
        for(int i = 0; i < max_key_length; i++) {

            string ctx = cyclic_shift(contents,i);
            int coincidences = 0;

            for(unsigned int j = 0; j < contents.length(); j++) {
                if(contents[j] == ctx[j]) {
                    coincidences++;
                }
            }

            if(coincidences > max_coincidences_amount && i > 0) {
                max_coincidences_columns = i;
                max_coincidences_amount = coincidences;
            }

            // cout << i << " - " << coincidences << endl;
         }

        cout << max_coincidences_columns << endl;

        // split it into columns
        vector<string> columns;
        vector<string> decoded_columns;

        // set up sizes
        columns.resize(max_coincidences_columns);

        // populate the columns
        for(unsigned int i = 0; i < contents.length(); i++) {
            columns[i % max_coincidences_columns] = columns[i % max_coincidences_columns] + contents.at(i);
        }

        for(int i = 0; i < max_coincidences_columns; i++) {
            cout << endl;
            cout << columns[i] << endl;
            int shift = get_caesar_shift(columns[i], false);
            cout << "shift: " << i << ", " << shift << endl;
            string answer = get_caesar_plaintext(columns[i], shift);
            decoded_columns.push_back(answer);
            cout << answer << endl;
        }

        string answer = "";
        int len = decoded_columns[0].length();
        for(int i = 0; i < len; i++) {
            string column = columns[i % max_coincidences_columns];
            answer = answer + column[i];
        }
        cout << endl;
        cout << answer << endl;
    }
    else if(shift > 0) {

        // SUBSTITUTION ------------------------------------------------
        cout << "shift: " << shift << endl;
        string answer = get_caesar_plaintext(contents, shift);
        cout << contents << endl;
        cout << answer << endl;
    }
    else {
        cout << "Hmm.. one-time pad?  something else?";
    }

    return 0;
}
